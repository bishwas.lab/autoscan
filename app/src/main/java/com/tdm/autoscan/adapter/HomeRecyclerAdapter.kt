package com.tdm.autoscan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.tdm.autoscan.R
import kotlinx.android.synthetic.main.home_item.view.*

class HomeItem(val primaryText: String,val secondaryText: String, val imageResource: Int, val fragment: Fragment)

class HomeRecyclerAdapter(private var items: List<HomeItem>) : RecyclerView.Adapter<HomeRecyclerAdapter.HomeItemViewHolder>() {

    private var clickListener: (HomeItem) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeItemViewHolder {
        return HomeItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.home_item, parent, false
            ) as LinearLayout
        )
    }

    override fun getItemCount(): Int = items.size

    fun purgedAdd(nItems: List<HomeItem>) {
        items = nItems
        notifyDataSetChanged()
    }

    fun setClickListener(newClickListener: (HomeItem) -> Unit) {
        clickListener = newClickListener
    }

    override fun onBindViewHolder(holder: HomeItemViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.home_item_img.setImageResource(item.imageResource)
        holder.itemView.home_card_title_primary_text.text = item.primaryText
        holder.itemView.home_card_title_secondary_text.text = item.secondaryText
        holder.itemView.home_card_view.setOnClickListener{ clickListener(item) }
        holder.itemView.card_arrow.setOnClickListener{ clickListener(item) }

    }

    inner class HomeItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}