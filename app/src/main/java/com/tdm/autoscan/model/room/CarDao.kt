package com.tdm.autoscan.model.room

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface CarDao {
    @Query("SELECT * FROM car_info")
    fun selectAll(): LiveData<List<Car>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
   suspend fun insert(car: Car)

    @Update
    fun update(car: Car)

    @Delete
    suspend fun delete(car:Car)

    @Query("SELECT * FROM car_info WHERE car_info.vin = :carVin")
    // the @Relation do the INNER JOIN for you ;)
     fun selectCarWithVin(carVin: String): Car?

    @Query("DELETE FROM car_info")
    fun deleteAll()
}