package com.tdm.autoscan.network

import android.util.Log
import com.tdm.autoscan.util.Constant
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.*

object API {

    const val URL = Constant.LOCAL_SERVER_ADDRESS + ":3000"

    object Model {
        // DTC response model
        data class DTCResponse(val code:String, val location:String, val probable_cause:String)
        data class RegionKMLResponse(val responseBody: ResponseBody, val regionName: String)
    }

    interface Service {
        @Headers("Content-Type: application/json")
        @POST("/api/dtc")
        fun uploadCode(@Body code: RequestBody): Call<List<Model.DTCResponse>>

        @GET("api/alert/regions/{regionName}")
        @Streaming
        fun downloadKMLData(@Path("regionName") regionName: String): Call<ResponseBody>

    }

    interface VinService {
        @POST("/api/vehicles/DecodeVINValuesBatch/{vin}")
        fun upload(@Path("vin") vin: String): Call<VINResponseModel.Response>
    }

    interface APIResponseCallback<T> {
        fun onSuccess(value: T)
        fun onFailure(throwable: Throwable)
    }

    private val retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val service: Service = retrofit.create(Service::class.java)

    private val vinRetrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(SimpleXmlConverterFactory.create())
        .build()
    val vinService: VinService = vinRetrofit.create(VinService::class.java)

    fun makeDTCRequest(code:String, callback: APIResponseCallback<Model.DTCResponse>) {

        val json = JSONObject("{\"code\":\"$code\"}").toString()
        Log.d("DBG", "json $json")

        val call = service.uploadCode( RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json))
        val value = object : Callback<List<Model.DTCResponse>> {
            override fun onResponse(call: Call<List<Model.DTCResponse>>, response: Response<List<Model.DTCResponse>>) {
                if (response.isSuccessful) {
                    val res = response.body()
                    if (res != null && res.isNotEmpty()){
                        val dtcResponse = res[0]
                        Log.d("DBG","code:---${code}, location:---${dtcResponse.location},probable_cause:---${dtcResponse.probable_cause}")
                        callback.onSuccess(dtcResponse)
                    } else
                        callback.onFailure(Throwable("Response null"))
                }

            }

            override fun onFailure(call: Call<List<Model.DTCResponse>>, t: Throwable) {
                Log.d("DBG", "upload failed ${t.message}")
                callback.onFailure(t)
            }


        }
        call.enqueue(value) // asynchronous request
    }

    fun downloadRegionKML(regionName: String,  callback: APIResponseCallback<Model.RegionKMLResponse>) {
        val call = service.downloadKMLData(regionName)

        val regionKML = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val res = response.body()
                if (res != null)
                    callback.onSuccess(Model.RegionKMLResponse(res, regionName))

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("DBG", "donwload failed faild ${t.message}")
                callback.onFailure(t)
            }
        }

        call.enqueue(regionKML)
    }

}