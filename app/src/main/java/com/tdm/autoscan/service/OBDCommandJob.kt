package com.tdm.autoscan.service

import com.github.pires.obd.commands.ObdCommand

class OBDCommandJob (val command: ObdCommand, val name: String, var state: OBDCommandJobState = OBDCommandJobState.NEW)

enum class OBDCommandJobState {NEW, RUNNING, FINISHED}