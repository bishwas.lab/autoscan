package com.tdm.autoscan.service

import com.github.pires.obd.commands.ObdCommand

class OBDCommandMock(private val min: Int, private val max: Int) : ObdCommand("") {
    override fun getName(): String {
        return ""
    }

    override fun getFormattedResult(): String {
        return (min..max).random().toString()
    }

    override fun performCalculations() {
    }

    override fun getCalculatedResult(): String {
        return ""
    }
}