package com.tdm.autoscan.util

object Constant {

    const val LOCAL_SERVER_ADDRESS = "http://192.168.1.110"

    const val MQTT_BROKER_ADDRESS = "tcp://192.168.1.110"
    const val MQTT_CLIENT_ID = "pixel_client"
    const val AMQP_ADDRESS = "192.168.1.110"

    // shared preference constants
    const val BLUETOOTH_DATA = "bluetooth_data"
    const val SELECTED_DEVICE_ADDRESS = "bluetooth_address"
    const val APP_DATA = "appData"
    const val ROAD_CONDITION_UPDATE_TIME = "roadConditionUpdateTime"
    const val SUBSCRIBED_REGIONS_KEY = "subbedRegions"

    // OBD constants
    const val CANNOT_CONNECT_TO_DEVICE = 1
    const val OBD_COMMAND_FAILURE = 10
    const val OBD_COMMAND_FAILURE_IO = 11
    const val OBD_COMMAND_FAILURE_UTC = 12
    const val OBD_COMMAND_FAILURE_IE = 13
    const val OBD_COMMAND_FAILURE_MIS = 14
    const val OBD_COMMAND_FAILURE_NODATA = 15

    // Shared preference constants
    const val PROFILE_PREFERENCE_FILE_KEY = "com.tdm.autoscan.util.ProfilePreference"


    const val PREFERENCE_VIN = "com.tdm.autoscan.view.CarProfileFragment"

    // Intent keys
    const val START_FRAGMENT = "StartFragment"

    // Å is replaced by A in Å land Island because firebase cloud message doesn't support the character in topic names
    val REGIONS: List<String> = listOf(
        "North Karelia",
        "Tavastia Proper",
        "south Ostrobothnia",
        "Satakunta",
        "Kainuu",
        "Ostrobothnia",
        "Northern Savonia",
        "Southern Savonia",
        "North Ostrobothnia",
        "Lapland",
        "Central Finland",
        "Aland Island",
        "Pirkanmaa",
        "South Karelia",
        "Central Ostrobothnia",
        "Uusimaa",
        "Southwest Finland",
        "Kymenlaakso"
    )

}