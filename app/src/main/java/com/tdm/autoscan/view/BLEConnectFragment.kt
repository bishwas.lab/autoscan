package com.tdm.autoscan.view


import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.RippleDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.skyfishjy.library.RippleBackground
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.tdm.autoscan.R
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.fragment_ble_connect.*
import kotlinx.android.synthetic.main.fragment_ble_connect.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BLEConnectFragment : BaseFragment() {
    private lateinit var pairedDevices: List<BluetoothDevice>
    private lateinit var ripple: RippleBackground
    private var connected = false
    private var snackBar: Snackbar? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_ble_connect, container, false)

        val bluetoothManager = activity?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

        pairedDevices =  bluetoothManager.adapter?.bondedDevices?.toList() ?: listOf()

        ripple = view.ripple_view as RippleBackground
        ripple.startRippleAnimation()

        return view
    }

    override fun onResume() {
        super.onResume()
        connect()
    }

    override fun onPause() {
        super.onPause()
        snackBar?.dismiss()
    }

    private fun showSnackBar(content: String, duration: Int, actionName: String, clickListener:(View) -> Unit ) {
        snackBar = Snackbar.make(activity?.findViewById(R.id.coordinator_layout)!!, content, duration)
            .setAction(actionName, clickListener)
        snackBar?.show()
    }

    private fun connect() {
        if (BluetoothConnection.socket != null && BluetoothConnection.socket!!.isConnected)
            configureConnection()
        else if (BluetoothConnection.deviceAddress == null) {
            connection_status_txt.text = "Searching..."
            showSnackBar(
                "Please select OBD2 device from settings.Click OK to go to settings",
                Snackbar.LENGTH_LONG,
                "OK"
            ) {startActivity(Intent(activity, SettingsActivity::class.java))}
            launch {
                pairedDevices.forEach {
                    BluetoothConnection.deviceAddress = it.address
                    if(BluetoothConnection.connect(activity!!)) {
                        configureConnection()
                        snackBar?.dismiss()
                        return@forEach
                    }
                }
                if (!connected){
                    showSnackBar(
                        "Could not find OBD 2 device please pair your device from android settings.",
                        Snackbar.LENGTH_INDEFINITE,
                        "PAIR"
                    ) {startActivity(Intent().setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS))}
                }
            }
        } else {
            connection_status_txt.text = "Connecting..."
            launch {
                if(BluetoothConnection.connect(activity!!))
                    configureConnection()
                else {
                    connection_status_txt.text = "Retrying connecting.."
                    delay(1000)
                    if (BluetoothConnection.connect(activity!!))
                        configureConnection()
                    else {
                        connection_status_txt.text = "Could not connect to device. Make sure device is near and turned on."
                        connection_status_txt.setTextColor(Color.RED)
                        showSnackBar(
                            "Please select OBD2 device from settings.Click OK to go to settings",
                            Snackbar.LENGTH_LONG,
                            "OK"
                        ) {startActivity(Intent(activity, SettingsActivity::class.java))}
                    }
                }
            }
        }
    }

    private fun configureConnection() {
        connection_status_txt.text = "Configuring..."
        launch {
            if (BluetoothConnection.configureSocket() != null) {
                connection_status_txt.text = "Connected"
                connection_status_txt.setTextColor(Color.GREEN)
                connected = true
                activity?.getSharedPreferences(Constant.BLUETOOTH_DATA, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.putString(Constant.SELECTED_DEVICE_ADDRESS, BluetoothConnection.deviceAddress )
                    ?.apply()
            }
            else {
                connection_status_txt.text = "Searching..."
            }
    }
}
}
