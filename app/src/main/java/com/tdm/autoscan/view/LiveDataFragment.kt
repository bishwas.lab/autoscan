package com.tdm.autoscan.view

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tdm.autoscan.R
import com.tdm.autoscan.service.OBDCommandJob
import com.tdm.autoscan.service.OBDCommandJobState
import com.tdm.autoscan.service.OBDService
import com.tdm.autoscan.util.CustomGauge
import com.tdm.autoscan.util.CustomGaugeItems
import com.tdm.autoscan.util.CustomTouchListener
import com.tdm.autoscan.viewModel.OBDLiveDataViewModel
import kotlinx.android.synthetic.main.custom_gauge_view.view.*
import kotlinx.android.synthetic.main.fragment_live_data.*
import java.util.concurrent.ConcurrentHashMap


class LiveDataFragment : BaseFragment(), AdapterView.OnItemClickListener {

    private lateinit var obdLiveDataViewModel: OBDLiveDataViewModel

    private val gaugeNames = CustomGaugeItems.customGauges.keys.toList()
    private lateinit var listPopupWindow: ListPopupWindow
    private var gauges: ConcurrentHashMap<String, CustomGauge> =  ConcurrentHashMap()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listPopupWindow = ListPopupWindow(context!!)
        listPopupWindow.setAdapter(ArrayAdapter(context!!, android.R.layout.simple_list_item_activated_1, gaugeNames))

        listPopupWindow.anchorView = fab
        listPopupWindow.width = 1000
        listPopupWindow.height = 1000
        listPopupWindow.isModal = true
        listPopupWindow.setOnItemClickListener(this)

        fab.setOnClickListener {
            listPopupWindow.show()
        }

        obdLiveDataViewModel = ViewModelProviders.of(this).get(OBDLiveDataViewModel::class.java)
        obdLiveDataViewModel.startService()
        obdLiveDataViewModel.obdCommandJob.observe(viewLifecycleOwner, Observer { job ->
            if (job != null && job.state == OBDCommandJobState.FINISHED) {
                Log.d("DBG", "oberser reading ${job.command.formattedResult.replace(Regex("[^\\d.]"), "")}")
                // remove non numeric characters
                val value = job.command.formattedResult.replace(Regex("[^\\d.]"), "")
                if (value.isNotEmpty())
                    gauges[job.name]?.gauge?.moveToValue(value.toFloat())
            }
        })

        if (OBDService.selectedJobs.isNotEmpty())
                OBDService.selectedJobs.keys.forEach {
                    drawGauge(it)
                }

    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        drawGauge(gaugeNames[position])
        listPopupWindow.dismiss()
    }

    private fun drawGauge(key: String) {

        // add gauge if it does not exist
        if (!gauges.containsKey(key)) {

            val v = LayoutInflater.from(context).inflate(R.layout.custom_gauge_view, live_data_container, false)
            live_data_container.addView(v)
            val gaugeView = v.custom_gauge_view
            val customGauge = CustomGaugeItems.customGauges[key]!!
            customGauge.gauge = gaugeView

            gaugeView.setTotalNicks(customGauge.totalNicks)
            gaugeView.setMinValue(customGauge.minValue)
            gaugeView.setMaxValue(customGauge.maxValue)
            gaugeView.setValuePerNick(customGauge.valuePerNick)
            gaugeView.setMajorNickInterval(customGauge.majorNickInterval)
            gaugeView.moveToValue(customGauge.moveToValue)
            gaugeView.setUpperText(customGauge.upperText)
            gaugeView.setLowerText(customGauge.lowerText)

            live_data_container.viewTreeObserver.addOnGlobalLayoutListener {
                gaugeView.setOnTouchListener(CustomTouchListener())
            }

            gauges[key] = customGauge
            obdLiveDataViewModel.addJob(OBDCommandJob(customGauge.command, key))

        } else {
            Toast.makeText(activity, "Item already exist.", Toast.LENGTH_LONG).show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        gauges = ConcurrentHashMap()
    }


}
