package com.tdm.autoscan.view


import android.content.Context
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.TimeZone
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tdm.autoscan.R
import com.tdm.autoscan.network.API
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.fragment_notification.view.*
import okhttp3.ResponseBody
import java.util.*

private const val OPEN_MAP = "param1"

class NotificationFragment : BaseFragment() {

    private var openMap = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            openMap = it.getBoolean(OPEN_MAP)
        }

        if (openMap)
            startActivity(Intent(activity, OSMActivity::class.java))
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_notification, container, false)

        val updateTime = activity?.getSharedPreferences(Constant.APP_DATA, Context.MODE_PRIVATE)
            ?.getString(Constant.ROAD_CONDITION_UPDATE_TIME, null)
        if (updateTime != null) {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
            parser.timeZone = TimeZone.getTimeZone("US")
            val date = parser.parse(updateTime)
            view.updateTime_txt.text = date.toString()
        }
        else {
            view.updateTime_txt.text = "No notification"
            view.show_alert_btn.isEnabled = false
        }

        view.show_alert_btn.setOnClickListener {
            startActivity(Intent(activity, OSMActivity::class.java))
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(openMap: Boolean) =
            NotificationFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(OPEN_MAP, openMap)
                }
            }
    }
}
