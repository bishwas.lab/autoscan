package com.tdm.autoscan.view

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.messaging.FirebaseMessaging
import com.tdm.autoscan.R
import com.tdm.autoscan.adapter.RegionSubscription
import com.tdm.autoscan.adapter.RegionsAdapter
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    private lateinit var regionSubscriptions: List<RegionSubscription>
    private var subscriptionChanged = false

    private lateinit var pairedDevices: List<BluetoothDevice>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        // expand or hide list of regions view when chevron icon is clicked
        region_card_arrow.setOnClickListener { arrowOnClick(regions_recyclerView, region_card_arrow) }

        // get subscribed set of regions from shared preference
        var subscribedRegions = getSharedPreferences(Constant.APP_DATA, Context.MODE_PRIVATE)
            .getStringSet(Constant.SUBSCRIBED_REGIONS_KEY, null)
        if (subscribedRegions == null)
            subscribedRegions = setOf()

        regionSubscriptions = Constant.REGIONS.map { RegionSubscription(it, subscribedRegions.contains(it)) }

        val adapter = RegionsAdapter(regionSubscriptions)
        adapter.setClickListener{ isChecked, regionSubscription ->
            regionSubscription.isSubscribed = isChecked
            regionSubscriptions = adapter.regionSubscriptions
            subscriptionChanged = true
        }
        regions_recyclerView.adapter = adapter
        regions_recyclerView.layoutManager = LinearLayoutManager(this)

        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        pairedDevices =  bluetoothManager.adapter?.bondedDevices?.toList() ?: listOf()

        // Add device name as radio buttons
        if (pairedDevices.isNotEmpty()) {
            pairedDevices.forEach {
                val radioBtn = RadioButton(this)
                radioBtn.id = View.generateViewId()
                radioBtn.text = it.name
                if (BluetoothConnection.deviceAddress == it.address)
                    radioBtn.isChecked = true
                devices_radio_grp.addView(radioBtn)
            }
        }

        // set selected device address
        devices_radio_grp.setOnCheckedChangeListener { radioGroup, position ->
            val checkedItem = radioGroup[position-1]
            if (checkedItem is RadioButton) {
                BluetoothConnection.deviceAddress = pairedDevices.find { it.name == checkedItem.text }?.address
            }
        }

        devices_card_arrow.setOnClickListener { arrowOnClick(devices_radio_grp, devices_card_arrow)}
    }

    override fun onDestroy() {

        // if region alert subscription changed
        if (subscriptionChanged) {
            // save set of region names which are subscribed to shared preference
            val subbedRegions = regionSubscriptions.filter { it.isSubscribed }.map { it.name }.toSet()
            getSharedPreferences(Constant.APP_DATA, Context.MODE_PRIVATE)
                .edit()
                .putStringSet(Constant.SUBSCRIBED_REGIONS_KEY, subbedRegions)
                .apply()

            // update topic subscription on Firebase
            try {
                val firebaseMessaging = FirebaseMessaging.getInstance()
                regionSubscriptions.forEach {
                    val topic = "weather-alert-${it.name.trim().replace(" ", "-")}"
                    val sub = if (it.isSubscribed)
                        firebaseMessaging.subscribeToTopic(topic)
                    else
                        firebaseMessaging.unsubscribeFromTopic(topic)
                    sub.addOnCompleteListener { task ->
                        if (!task.isSuccessful)
                            Log.d("DBG", "sub task not successful: ${task.exception}")
                    }
                }
            } catch (e: Exception) {
                Log.d("DBG", "Error while subscribing to firebase topics ${e.message}")
                e.printStackTrace()
            }
        }

        // save selected device address
        if (BluetoothConnection.deviceAddress != null) {
            getSharedPreferences(Constant.BLUETOOTH_DATA, Context.MODE_PRIVATE)
                ?.edit()
                ?.putString(Constant.SELECTED_DEVICE_ADDRESS, BluetoothConnection.deviceAddress )
                ?.apply()
        }

        super.onDestroy()
    }

    private fun arrowOnClick(view: View, arrow: View) {
        if ( view.visibility == View.GONE) {
            view.visibility =  View.VISIBLE
            arrow.animate().rotation(180f).start()
        } else {
            view.visibility =  View.GONE
            arrow.animate().rotation(0f).start()
        }
    }
}
