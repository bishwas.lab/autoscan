package com.tdm.autoscan.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.tdm.autoscan.adapter.HomeItem
import com.tdm.autoscan.model.AppRepository

class HomeViewModel : ViewModel() {

    val items: LiveData<MutableList<HomeItem>> = AppRepository.homeGridItems
}